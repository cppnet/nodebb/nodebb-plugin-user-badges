# nodebb-plugin-user-badges

![nodebb compatibility](https://packages.nodebb.org/api/v1/plugins/nodebb-plugin-user-badges/compatibility.png)

This plugin is in very early release and only has very limited features yet. The target is to provide the possibility to assign badges to single users that work similar to group badges.

At the moment it is only possible to automatically display a bad for moderators in their assigned categories. There is minimal customization for the moderator badge in the administrator settings of this plugin.