# Changelog

## v0.3.0 - 2019-03-01

* Added option to display mod label as group label
* Fixed problem of saving initial settings in the wrong key

## v0.2.0 - 2018-12-28

* Fixed warnings from NodeBB 1.11
* Fixed settings not initializing properly after first install

## v0.1.0 - 2018-10-27

* Initial release
