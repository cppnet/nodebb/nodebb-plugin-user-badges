const topics = require.main.require('./src/topics');
const privileges = require.main.require('./src/privileges');
const meta = require.main.require('./src/meta');
const winston = require.main.require('winston');

const async = require('async');

let pluginSettings = {};

exports.init = function (params, callback) {
    params.router.get('/admin/plugins/user-badges', params.middleware.admin.buildHeader, renderAdmin);
    params.router.get('/api/admin/plugins/user-badges', renderAdmin);

    meta.settings.get('nodebb-plugin-user-badges', function (err, settings) {
        if (err) {
            return callback(err);
        }

        function initSettingProperty(property, defaultValue) {
            if (!settings.hasOwnProperty(property)) {
                settings[property] = defaultValue;
            }
        }

        initSettingProperty('displayModBadge', 'on');
        initSettingProperty('displayAsGroupBadge', 'on');
        initSettingProperty('displayModBadgeColor', "#db001d");
        initSettingProperty('displayModBadgeIcon', "fa-gavel");
        initSettingProperty('displayModBadgeText', "Mod");

        pluginSettings = settings;
        meta.settings.set('nodebb-plugin-user-badges', settings, callback);
    });
};

exports.addAdminNavigation = function (header, callback) {
    header.plugins.push({
        route: '/plugins/user-badges',
        icon: 'fa-id-badge',
        name: 'User Badges'
    });

    callback(null, header);
};

exports.updatePluginSettings = function (data) {
    if (data.plugin === 'nodebb-plugin-user-badges') {
        pluginSettings = data.settings;
    }
};

exports.addPostData = function (data, callback) {
    if (pluginSettings.displayModBadge !== 'on') {
        callback(null, data);
        return;
    }

    const posts = data.posts;
    if (posts.length === 0) {
        callback(null, data);
        return;
    }

    topics.getTopicField(posts[0].tid, 'cid', function (err, cid) {
        if (err) {
            winston.error('[plugin/user-badges] Failed to retrieve category id!', err);
            return;
        }

        const users = new Map();
        posts.forEach(post => {
            if (!users.has(post.uid)) {
                users.set(post.uid, post.user);
            }
        });

        let appendModIcon;
        if (pluginSettings.displayAsGroupBadge === 'on') {
            appendModIcon = function (userData) {
                userData.selectedGroups.push({
                    name: pluginSettings.displayModBadgeText,
                    slug: "../user/" + userData.userslug,
                    labelColor: pluginSettings.displayModBadgeColor,
                    icon: pluginSettings.displayModBadgeIcon,
                    userTitle: pluginSettings.displayModBadgeText,
                });
            };
        } else {
            appendModIcon = function (userData) {
                userData.custom_profile_info.push({
                    content: '<span class="badge rounded-1 text-uppercase text-truncate"' +
                        ' style="background-color:' + pluginSettings.displayModBadgeColor + ';">' +
                        '<i class="fa ' + pluginSettings.displayModBadgeIcon + '"></i>' +
                        '&nbsp;<span class="badge-text">' + pluginSettings.displayModBadgeText + '</span></span>'
                });
            }
        }

        async.each(users.keys(), function (uid, next) {
            privileges.users.isModerator(uid, cid, function (err, isModerator) {
                if (!err && isModerator) {
                    appendModIcon(users.get(uid));
                }

                next(err);
            })
        }, function (err) {
            if (err) {
                winston.error('[plugin/user-badges] Failed to retrieve moderator status!', err);
                return;
            }

            callback(null, data);
        });
    });
};

function renderAdmin(req, res) {
    res.render('admin/plugins/user-badges', {});
}
