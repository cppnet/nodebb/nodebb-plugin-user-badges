<div class="acp-page-container">
    <!-- IMPORT admin/partials/settings/header.tpl -->

    <div class="row m-0">
        <div id="spy-container" class="col-12 col-md-8 px-0 mb-4" tabindex="0">
            <form role="form" class="form nodebb-plugin-user-badges-settings" id="nodebb-plugin-user-badges-settings">
                <div class="mb-4">
                    <h5 class="fw-bold tracking-tight settings-header">
                        [[nodebb-plugin-user-badges:moderator-badge-title]]
                    </h5>

                    <div class="mb-3 form-check form-switch">
                        <input class="form-check-input" type="checkbox" id="displayModBadge" name="displayModBadge">
                        <label class="form-check-label" for="displayModBadge">
                            [[nodebb-plugin-user-badges:display-moderator-badge]]
                        </label>
                    </div>

                    <div class="mb-3 form-check form-switch">
                        <input class="form-check-input" type="checkbox" id="displayAsGroupBadge" name="displayAsGroupBadge">
                        <label class="form-check-label" for="displayAsGroupBadge">
                            [[nodebb-plugin-user-badges:display-as-group-badge]]
                        </label>
                    </div>

                    <div class="mb-3">
                        <label class="form-label" for="displayModBadgeColor">
                            [[nodebb-plugin-user-badges:moderator-badge-color]]
                        </label>
                        <input class="form-control" type="color" id="displayModBadgeColor" name="displayModBadgeColor">
                    </div>

                    <div class="mb-3">
                        <label class="form-label" for="displayModBadgeIcon">
                            [[nodebb-plugin-user-badges:moderator-badge-icon]]
                        </label>
                        <input class="form-control" type="text" id="displayModBadgeIcon" name="displayModBadgeIcon">
                    </div>

                    <div class="mb-3">
                        <label class="form-label" for="displayModBadgeText">
                            [[nodebb-plugin-user-badges:moderator-badge-text]]
                        </label>
                        <input class="form-control" type="text" id="displayModBadgeText" name="displayModBadgeText">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- IMPORT admin/partials/settings/toc.tpl -->
</div>
