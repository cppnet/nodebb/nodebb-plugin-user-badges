'use strict';

define('admin/plugins/user-badges', ['settings'], function (settings, colorPicker) {
    let userBadges = {};

    userBadges.init = function() {
        settings.load('nodebb-plugin-user-badges', $('#nodebb-plugin-user-badges-settings'), function (err, settings) {
            const defaults = {
                displayModBadge: false,
                displayAsGroupBadge: false,
                displayModBadgeColor: '#ff0000',
                displayModBadgeIcon: 'fa-sword',
                displayModBadgeText: 'Moderator'
            };

            for(const setting in defaults) {
                if (!settings.hasOwnProperty(setting)) {
                    if (typeof defaults[setting] === 'boolean') {
                        $('#' + setting).prop('checked', defaults[setting]);
                    } else {
                        $('#' + setting).val(defaults[setting]);
                    }
                }
            }
        });

        $('#save').on('click', function() {
            settings.save('nodebb-plugin-user-badges', $('#nodebb-plugin-user-badges-settings'));
        });
    };

    return userBadges;
});
